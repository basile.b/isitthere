module formaters.pascal;

import
    std.stdio, std.conv, formaters, std.array, std.format;

/**
 * A formater that writes a Pascal record.
 *
 * The presence of a word in the dictionary is made with the `contains` function.
 */
final class PascalFormater: Formater
{
    private string _name;

    ///
    this(A...)(A a)
    {
        super(a);
    }

    override void begins(string name)
    {
        _name = name;
        file.writeln("{$MODESWITCH ADVANCEDRECORDS}");
        file.writeln("type ", name , " = record");
        file.writeln("private");
        file.writeln();
    }

    override void writeRenderInformation(string renderInformation)
    {
        if (renderInformation.length)
        {
            file.writeln("  {");
            file.writeln(renderInformation);
            file.writeln("  }");
            file.writeln();
        }
    }

    override void writeDictionary(const string[] dictionary, size_t size)
    {
        file.writeln("  const fWords: array [0..", size-1, "] of string =");
        char[] array = format("%s", dictionary).replace("\"", "'").dup;
        array[0] = '(';
        array[$-1] = ')';
        file.writeln("    ", array, ";");
        file.writeln();
    }

    override void writeOccupiedBuckets(const bool[] occupiedBuckets, size_t size)
    {
        file.writeln("  const fFilled: array [0..", size-1, "] of boolean =");
        char[] array = format("%s", occupiedBuckets).dup;
        array[0] = '(';
        array[$-1] = ')';
        file.writeln("    ", array, ";");
        file.writeln();
    }

    override void writeCoefficients(const ref ubyte[256] coefficients)
    {
        file.writeln("  const fCoefficients: array [0..255] of Byte = ");
        char[] array = format("%s", coefficients).dup;
        array[0] = '(';
        array[$-1] = ')';
        file.writeln("    ", array, ";");
        file.writeln();
    }

    override void writeTestFunction(size_t size, bool caseCare)
    {
        file.writeln("  class function hash(const w: string): Word; static;");
        file.writeln("public");
        file.writeln("  class function contains(const w: string): boolean; static;");
        file.writeln("end;");
        file.writeln();


        const string hashStr =
"{$PUSH}{$R-}{$COPERATORS ON}
class function "~ _name ~ ".hash(const w: string): Word;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to length(w) do
    Result += fCoefficients[Byte(w[i])];
  Result := Result mod " ~ to!string(size) ~ ";
end;
{$POP}
";
        file.writeln(hashStr);

        const string containsStrCC =
"class function "~ _name ~ ".contains(const w: string): boolean;
var
  h: Word;
begin
  result := false;
  h := hash(w);
  if fFilled[h] then
    result := fWords[h] = w;
end;";

        const string containsStrCDC =
"class function "~ _name ~ ".contains(const w: string): boolean;
var
  h: Word;
  s: string;
begin
  result := false;
  s := LowerCase(w);
  h := hash(s);
  if fFilled[h] then
    result := fWords[h] = s;
end;";

        if (caseCare)
            file.writeln(containsStrCC);
        else
            file.writeln(containsStrCDC);
    }

    override void ends(){}
}

