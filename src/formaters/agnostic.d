module formaters.agnostic;

import
    std.stdio, formaters;

/**
 * A simple formater, not specialized for any programming language.
 */
final class AgnosticFormater: Formater
{
    ///
    this(A...)(A a)
    {
        super(a);
    }

    override void begins(string name){}

    override void writeRenderInformation(string renderInformation)
    {
        file.writeln("Information: ");
        file.writeln(renderInformation);
        file.writeln;
    }

    override void writeDictionary(const string[] dictionary, size_t size)
    {
        file.writeln("reorganized dictionary (string[", size, "]):");
        file.writeln(dictionary);
        file.writeln;
    }

    override void writeOccupiedBuckets(const bool[] occupiedBuckets, size_t size)
    {
        file.writeln("bucket filled (bool[", size, "]):");
        file.writeln(occupiedBuckets);
        file.writeln;
    }

    override void writeCoefficients(const ref ubyte[256] coefficients)
    {
        file.writeln("hash coefficients (ubyte[256]):");
        file.writeln(coefficients);
        file.writeln;
    }

    override void writeTestFunction(size_t size, bool caseCare)
    {}

    override void ends(){}
}

