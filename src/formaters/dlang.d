module formaters.dlang;

import
    std.stdio, std.conv, formaters;

/**
 * A formater that writes a D structure.
 *
 * The presence of a word in the dictionary is made with the `in` operator.
 */
final class DlangFormater: Formater
{
    ///
    this(A...)(A a)
    {
        super(a);
    }

    override void begins(string name)
    {
        file.writeln("struct ", name);
        file.writeln("{");
        file.writeln();
        file.writeln("private:");
        file.writeln();
    }

    override void writeRenderInformation(string renderInformation)
    {
        if (renderInformation.length)
        {
            file.writeln("    /*");
            file.writeln(renderInformation);
            file.writeln("    */");
            file.writeln();
        }
    }

    override void writeDictionary(const string[] dictionary, size_t size)
    {
        file.writeln("    static const string[", size ,"] _words = ", dictionary, ";");
        file.writeln();
    }

    override void writeOccupiedBuckets(const bool[] occupiedBuckets, size_t size)
    {
        file.writeln("    static const bool[", size ,"] _filled = ", occupiedBuckets, ";");
        file.writeln();
    }

    override void writeCoefficients(const ref ubyte[256] coefficients)
    {
        file.writeln("    static const ubyte[256] _coefficients = ", coefficients, ";");
        file.writeln();
    }

    override void writeTestFunction(size_t size, bool caseCare)
    {
        const string hashStr =
    "    static ushort hash(const char[] word) nothrow pure @safe @nogc
    {
        ushort result;
        foreach(i; 0..word.length)
        {
            result += _coefficients[word[i]];
        }
        return result % " ~ to!string(size) ~ ";";
        file.writeln(hashStr);
        file.writeln("    }");
        file.writeln();
        file.writeln("public:");
        file.writeln();

        const string opInStrCC =
    "    static bool opBinaryRight(string op: \"in\")(const char[] word)
    {
        bool result;
        const ushort h = hash(word);
        if (_filled[h])
            result = _words[h] == word;
        return result;
    }";

        const string opInStrCDC =
    "    static bool opBinaryRight(string op: \"in\")(const char[] word)
    {
        import std.uni : toLower;
        bool result;
        auto w = word.toLower;
        const ushort h = hash(w);
        if (_filled[h])
            result = _words[h] == w;
        return result;
    }";

        if (caseCare)
            file.writeln(opInStrCC);
        else
            file.writeln(opInStrCDC);
    }

    override void ends()
    {
        file.writeln("}");
    }
}

