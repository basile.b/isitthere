module processing;

import
    std.stdio, std.random;
import
    core.thread, core.sync.mutex, core.atomic;

/**
 * Data used by the threaded finders.
 */
struct Parameters
{
    /// Coefficients used by the hashing function.
    ubyte[256] coefficients;

    /// Gives the index of each word in the original dictionary.
    ptrdiff_t[] dictionaryIndex;

    ///
    this(this) nothrow @safe
    {
        coefficients = coefficients.dup;
        dictionaryIndex = dictionaryIndex.dup;
    }

    ///
    bool valid() nothrow @safe
    {
        return dictionaryIndex.length != 0;
    }
}

/// Parameters filled when a hashset is found.
__gshared Parameters results;

/**
 * The Filler is a Mutex that wraps a PRNG used by the finders
 * to get new coefficients without race conditions.
 */
final class Filler(T) : Mutex
{

private:

    uint _index;
    uint _pfCount;
    uint _pf;
    T _rng;

public:

    @disable this();

    /// Constructs with an initial seed.
    this(uint seed, uint periodFactor) @safe
    {
        if (!seed)
            seed = unpredictableSeed;

        _pf = periodFactor == 0 ? 1 : periodFactor;

        _rng = T(seed);
    }

    /// Fills coefficients with 256 new values.
    void fill(ref ubyte[256] coefficients) @nogc @trusted nothrow
    {
        foreach (immutable i; 0 .. 256 / 4)
        {
            *cast(uint*) &coefficients[i * 4] = _rng.front();
            _rng.popFront();
            _index++;
        }
        if (_index == 0)
        {
            _pfCount++;
            if (_pfCount == _pf)
                atomicStore(done, true);
        }
    }
}

/// The filler instance used by the program.
__gshared Filler!Mt19937 filler;

/// The flag used to stop processing
shared(bool) done;

/**
 * Encapsulates the data and the function for a thread.
 */
final class Finder
{

private:

    immutable size_t _size;
    immutable string[] _dictionary;
    Parameters _parameters;

    ushort hash(ref const string word) nothrow @nogc @safe
    {
        ushort h;
        foreach (immutable i; 0 .. word.length)
        {
            h += _parameters.coefficients[word[i]];
        }
        return h % _size;
    }

public:

    @disable this();

    ///
    this(string[] dictionary, size_t size) @safe nothrow
    {
        _size = size;
        _dictionary = dictionary.idup;
        _parameters.dictionaryIndex.length = _size;
    }

    /// Method used by a processing thread.
    void find()
    {
        ubyte[] dups;
        dups.length = _size;

        while (true)
        {

            _L0:

            if (atomicLoad(done))
            {
                debug writeln("another thread has found the results");
                return;
            }

            synchronized(filler)
            {
                filler.fill(_parameters.coefficients);
            }

            dups[] = 0;
            _parameters.dictionaryIndex[] = -1;

            foreach (i, word; _dictionary)
            {
                const ushort h = hash(word);
                _parameters.dictionaryIndex[h] = i;
                if (dups[h]++)
                    goto _L0;
            }

            break;
        }

        if (atomicLoad(done))
        {
            debug writeln("another thread has found the results");
            return;
        }
        atomicStore(done, true);

        results = _parameters;

        debug writeln("found the results");
    }
}

