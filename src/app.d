module isithere;

import
    core.thread, std.stdio, std.path, std.string, std.random, std.file,
    std.format, std.datetime;
import
    processing, formaters;

void main(string[] args)
{

    string iFile, oFile;
    uint initSeed;
    ubyte numThreads;
    string formaterKind;
    string formatedName;
    size_t mapLength;
    string[] dictionary;
    bool cc = true;
    ubyte rk;
    uint period;

    import std.getopt: getopt, config, defaultGetoptPrinter;

    auto r = getopt(args, config.passThrough | config.stopOnFirstNonOption,
        "cc", ": case care, by default true", &cc,
        "fk", ": the kind of the formater used to write the results", &formaterKind,
        "fn", ": the name of the class produced by the formater", &formatedName,
        "if", ": the file that contains the words, one by line",  &iFile,
        "is", ": the initial seed, optional, 0 means unpredictable", &initSeed,
        "ml", ": the length of the map, optional", &mapLength,
        "nt", ": the count of threads used, optional", &numThreads,
        "of", ": the name of the output file, optional, when not set stdout is used",  &oFile,
        "pf", ": the period factor, optional. Multiply by 1<<32 to get the actuall period", &period,
        //"rk", ": the PRNG kind, 0 means Mersenne Twister, 1 means XorShift", &rk
    );
    if (r.helpWanted)
    {
        defaultGetoptPrinter("options", r.options);
        return;
    }

    debug
    {
        formaterKind = "d";
        formatedName = "DlangKeywords";
        iFile = "test/d2kw.txt";
        oFile = "test/d2kw.d";
        numThreads = 4;
        initSeed = 256;
        mapLength = 512;
    }

    if (!iFile.exists)
    {
        if (iFile.length)
            writeln(iFile, " is not a valid input file");
        else
            writeln("a file must be specified with the `--if=` switch.");
        return;
    }
    if (oFile == iFile)
    {
        import std.path: stripExtension;
        oFile = iFile.stripExtension ~ "-tables.txt";
    }

    with (File(iFile))
    {
        if (cc)
        {
            foreach(line; byLine)
            {
                string w = line.strip.idup;
                if (w.length)
                    dictionary ~= w;
            }
        }
        else
        {
            foreach(line; byLine)
            {
                import std.uni;
                char[] w = line.strip;
                if (w.length)
                {
                    toLowerInPlace(w);
                    dictionary ~= w.idup;
                }
            }
        }
        close;
    }
    if (dictionary.length < 2)
    {
        writeln("not enough words: ", dictionary);
        return;
    }
    if (dictionary.length > 0xFFFF / 2)
    {
        writeln("too much words: ", dictionary.length, " > ", 0xFFFF / 2);
        return;
    }
    if (mapLength < dictionary.length)
    {
        mapLength = dictionary.length * 2;
        writeln("map length increased to: ", mapLength);
    }
    if (!formatedName.length)
    {
        formatedName =  iFile.baseName.stripExtension;
        writeln("missing hashset name (--fn) set to: ", formatedName);
    }

    if (!numThreads)
        numThreads++;

    filler = new Filler!Mt19937(initSeed, period);

    import core.thread, core.atomic;

    Finder[] finders;
    Thread[] threads;
    foreach(immutable i; 0..numThreads)
    {
        finders ~= new Finder(dictionary, mapLength);
        threads ~= new Thread(&finders[i].find);
        threads[$-1].start;
    }

    while (true)
    {
        Thread.sleep(dur!"msecs"(100));
        if (atomicLoad(done))
            break;
    }

    if (results.valid)
    {
        Formater f;
        switch(formaterKind)
        {
            default: f = new AgnosticFormater(oFile); break;
            case "":
            case"0":
            case "agnostic": f = new AgnosticFormater(oFile); break;
            case "1":
            case "d":
            case "dlang" : f = new DlangFormater(oFile); break;
            case "2":
            case "pas":
            case "pascal" : f = new PascalFormater(oFile); break;
        }

        f.begins(formatedName);

        string renderInformation = format(
        "        rendered on %s by IsItThere.
         - PRNG seed: %s
         - map length: %d
         - case sensitive: %s", Clock.currTime.toString, initSeed, mapLength, cc);
        f.writeRenderInformation(renderInformation);

        string[] reorganizedDictionary = new string[](mapLength);
        foreach(immutable i; 0..mapLength)
        {
            const ptrdiff_t j = results.dictionaryIndex[i];
            if (j > -1)
                reorganizedDictionary[i] = dictionary[j];
        }
        f.writeDictionary(reorganizedDictionary, mapLength);

        bool[] occupiedBuckets = new bool[](mapLength);
        foreach(immutable i; 0..mapLength)
            occupiedBuckets[i] = results.dictionaryIndex[i] != -1;
        reorganizedDictionary.length = mapLength;
        f.writeOccupiedBuckets(occupiedBuckets, mapLength);

        f.writeCoefficients(results.coefficients);

        f.writeTestFunction(mapLength, cc);

        f.ends();
    }
    else
    {
        write("No coefficients found for the input dictionary. ");
        write("Try with a bigger map length, a bigger period factor or another initial seed.\n");
    }
}

